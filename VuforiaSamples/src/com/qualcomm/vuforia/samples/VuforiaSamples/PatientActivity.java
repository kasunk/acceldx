package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class PatientActivity  extends DatabaseInvokingActivity implements OnClickListener {

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.askpatient);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Button mButton = (Button)findViewById(R.id.forward);
		mButton.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		final EditText data = (EditText)findViewById(R.id.patientID);
		Future future=executor.submit(new Callable<Boolean>() {
			public Boolean call() {
					try {
						SharefileAPI.api().initialiseUser(data.getText().toString());
						return true;
					}
					catch(Exception e) {
						return false;
					}
			}
		});
		try {
			Boolean result=(Boolean) future.get();
			if (result) {
				Intent intent = new Intent(getBaseContext(),
						   Dashboard.class);
				startActivity(intent);
				finish();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
