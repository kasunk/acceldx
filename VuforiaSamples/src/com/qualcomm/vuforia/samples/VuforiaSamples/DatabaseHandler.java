package com.qualcomm.vuforia.samples.VuforiaSamples;
import java.io.File;
import java.io.IOException;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "dataStore";
 
    private static final String USERS_NAME = "userid";
	
    // Data reading store
    private static final String TABLE_DATA = "data";
    // Data table name
    private static final String DATA_TIMESTAMP = "ts";
    private static final String DATA_READING_A = "valueA";
    private static final String DATA_READING_B = "valueB";
    private static final String DATA_TRANSMITTED = "transmitted";

    // Image store
    private static final String TABLE_IMAGE = "images";
    // Image table name
    private static final String IMAGE_TIMESTAMP = "ts";
    private static final String IMAGE_PATH = "path";
    private static final String IMAGE_TRANSMITTED = "transmitted";

    private final SharedPreferences sharedPref;
    
    public DatabaseHandler(Context context, SharedPreferences prefs) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        sharedPref=prefs;
    }
    
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_DATA = "CREATE TABLE " + TABLE_DATA + "("
        		+ USERS_NAME + " TEXT,"
        		+ DATA_TIMESTAMP + " INTEGER,"
                + DATA_READING_A + " REAL,"
                + DATA_READING_B + " REAL,"
                + DATA_TRANSMITTED + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_DATA);

        String CREATE_IMAGE_DATA = "CREATE TABLE " + TABLE_IMAGE + "("
        		+ USERS_NAME + " TEXT,"
        		+ IMAGE_TIMESTAMP + " INTEGER,"
                + IMAGE_PATH + " TEXT,"
                + IMAGE_TRANSMITTED + " INTEGER" + ")";
        db.execSQL(CREATE_IMAGE_DATA);
    }
    
 // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE);
        // Create tables again
        onCreate(db);
    }
    
    // register a user
    public void createUser(String userName, String password) {
    	SharedPreferences.Editor editor = sharedPref.edit();
    	editor.putString("currentUser", userName);
        editor.putString(userName, password);
        editor.commit();
    }
    
    public String getUsername() {
    	String defaultVal=""+System.currentTimeMillis();
    	String userName=sharedPref.getString("currentUser", defaultVal);
    	if (userName.compareTo(defaultVal)==0) {
    		return null;
    	}
    	return userName;
    }

    public String getDeviceID() {
    	String myGuid=java.util.UUID.randomUUID().toString();
    	String saved=sharedPref.getString("device", myGuid);
    	if (saved.compareTo(myGuid)==0) {
    		SharedPreferences.Editor editor = sharedPref.edit();
        	editor.putString("device", myGuid);
            editor.commit();
    	}
    	return saved;
    }
    
    private void saveFile(String userName, long ts, File image) throws IOException {
    	ContentValues values = new ContentValues();
        values.put(USERS_NAME, userName); // Contact Name
        values.put(IMAGE_TIMESTAMP, ts); // Contact Phone Number
        values.put(IMAGE_PATH, image.getCanonicalPath()); // Contact Phone Number
        values.put(IMAGE_TRANSMITTED, 0);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_IMAGE, null, values);
        db.close(); // Closing database connection
    }
    
    private void saveData(String userName, long ts, double dataA, double dataB) {
    	ContentValues values = new ContentValues();
        values.put(USERS_NAME, userName); // Contact Name
        values.put(DATA_TIMESTAMP, ts); // Contact Phone Number
        values.put(DATA_READING_A, dataA); // Contact Phone Number
        values.put(DATA_READING_B, dataB); // Contact Phone Number
        values.put(DATA_TRANSMITTED, 0);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DATA, null, values);
        db.close(); // Closing database connection
    }
    
    
    // add data
    public void addData(File image, long ts, double num1, double num2) throws Exception {
    	String userName=getUsername();
    	if (userName!=null) {
    		saveData(userName, ts, num1, num2);
    		saveFile(userName, ts, image);
    	}
    }
    
    public double getMinOver(long range) {
    	return 100-Math.random()*20;
    }
    
    public double getMaxOver(long range) {
    	return 120+Math.random()*20;
    }	
    public double getAvgOver(long range) {
    	return 100+Math.random()*10;
    }
}
