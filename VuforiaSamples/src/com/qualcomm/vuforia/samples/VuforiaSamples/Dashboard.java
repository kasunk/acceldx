package com.qualcomm.vuforia.samples.VuforiaSamples;
import com.qualcomm.vuforia.samples.VuforiaSamples.app.ImageTargets.ImageTargets;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;

public class Dashboard extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		setTitle("Please choose option");
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Button takeTest = (Button)findViewById(R.id.btn_taketest);
		takeTest.setOnClickListener(new TestStarter());
		takeTest = (Button)findViewById(R.id.btn_histry);
		takeTest.setOnClickListener(new HistoryStarter());
	}

	class TestStarter implements OnClickListener {
	@Override
	public void onClick(View source){
		Intent intent = new Intent(getBaseContext(),
	   				   				   InstActivity.class);
		startActivity(intent);
		}
	}
	
	class HistoryStarter implements OnClickListener {
		@Override
		public void onClick(View source) {
			Intent intent = new Intent(getBaseContext(),
		   				   				   History.class);
			startActivity(intent);
			}
	}
}
