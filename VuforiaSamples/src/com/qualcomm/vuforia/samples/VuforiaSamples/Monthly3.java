package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.util.ArrayList;
import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;


import android.os.Bundle;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class Monthly3 extends DatabaseInvokingActivity {

	private GraphicalView mChart;
	

    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

    private XYSeries minSeries;
    private XYSeries maxSeries;

    private XYSeriesRenderer mCurrentRenderer;
    
    private static StringBuilder before=null;
    private static StringBuilder after=null;
	
    private void initChart() {
        minSeries = new XYSeries("Average BNP");
        mDataset.addSeries(minSeries);
        maxSeries = new XYSeries("Max Value");
       // mDataset.addSeries(maxSeries);
        mCurrentRenderer = new XYSeriesRenderer();
        mRenderer.addSeriesRenderer(mCurrentRenderer);
    }

    private void addSampleData() {
    	DatabaseHandler db=getDB();
    	for (int i=1; i<5; i++) {
    		minSeries.add(i, db.getAvgOver(getRange()));
            maxSeries.add(i, db.getMinOver(getRange()));
    	}
    }
    
    private static void initialiseBefore() {
    	before=new StringBuilder();
    	before.append("\n <html> ");
    	before.append("\n <head> ");
    	before.append("\n 	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'> ");
    	before.append("\n 	<title>Flot Examples: Interactivity</title> ");
    	before.append("\n 	<script language='javascript' type='text/javascript' src='http://code.jquery.com/jquery-1.8.3.min.js'></script> ");
    	before.append("\n 	<script language='javascript' type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/flot/0.8.2/jquery.flot.min.js'> ");
    	before.append("\n 	</script> ");
    	before.append("\n<script language='javascript' type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/flot/0.8.2/jquery.flot.time.min.js'></script>");
    	before.append("\n<script language='javascript' type='text/javascript' src='http://cdnjs.cloudflare.com/ajax/libs/flot/0.8.2/jquery.flot.threshold.min.js'></script>");

    	before.append("\n 	<script type='text/javascript'> ");
    	before.append("\n  ");
    	before.append("\n 	$(function() { ");
    	before.append("\n   var plotMe=[];");
    }
    
    private static void initialiseAfter() {
    	after=new StringBuilder();
    	after.append("\n  ");
    	after.append("\n 		for (var i = 0; i < data.length; i++) { ");
    	after.append("\n 			plotMe.push([ts[i], data[i]]); ");
    	after.append("\n 		} ");
    	after.append("\n  ");
    	after.append("\n var maximum=data.reduce(function(current, candidate) { if (current<candidate) {current=candidate;} return current; }, -1);");
    	after.append("\n var amber=maximum*0.8");
		after.append("\n var red=maximum*0.6;    	");
		after.append("\n 		var plot = $.plot('#placeholder', [ ");
    	after.append("\n 			{ data: plotMe, color: 'rgb(200, 20, 30)', threshold: [{ below: amber, color: 'rgb(255, 190, 0)'}, { below: red, color: 'rgb(30, 180, 20)'}]}, ");
    	after.append("\n 		], { ");
    	after.append("\n 			series: { ");
    	after.append("\n 				lines: { ");
    	after.append("\n 					show: true ");
    	after.append("\n 				}, ");
    	after.append("\n 				points: { ");
    	after.append("\n 					show: true ");
    	after.append("\n 				} ");
    	after.append("\n 			}, ");
    	after.append("\n 			grid: { ");
    	after.append("\n 				hoverable: true, ");
    	after.append("\n 				clickable: true,  color: 'white', backgroundColor: '#2F4F4F' ");
    	after.append("\n 			}, ");
    	after.append("\n 			yaxis: { ");
    	after.append("\n 				min: 50, ");
    	after.append("\n 				max: 200, color: 'white' ");
    	after.append("\n 			}, xaxis: { mode: 'time',  color: 'white', timeformat: '%m/%d/%y',   minTickSize: [1, 'day']}");
    	after.append("\n 		}); ");
    	after.append("\n  ");
    	after.append("\n  ");
    	after.append("\n 	}); ");
    	after.append("\n  ");
    	after.append("\n 	</script> ");
    	after.append("\n </head> ");
    	after.append("\n <body background='file:///android_asset/background.png'> ");
    	after.append("\n 	<div id='header'> ");
    	after.append("\n 		<h2 style='color:white;'>BNP Level (pg/mL)</h2> ");
    	after.append("\n 	</div> ");
    	after.append("\n  ");
    	after.append("\n 	<div id='content'> ");
    	after.append("\n  ");
    	after.append("\n 		<div class='demo-container'> ");
    	after.append("\n 			<div id='placeholder' class='demo-placeholder' style='width:100%;height:80%'></div> ");
    	after.append("\n 		</div> ");
    	after.append("\n 	</div>");
    	after.append("\n </body> ");
    	after.append("\n </html> ");
    	after.append("\n  ");
    }
    
    private String generatePlot(long[] timestamps, double[] toplot) {
    	StringBuilder data=new StringBuilder();
    	if (before==null) {
    		initialiseBefore();
    	}
    	if (after==null) {
    		initialiseAfter();
    	}
    	data.append(before.toString());
    	data.append("\n 		var data = [");
    	for (int i=0; i<toplot.length; i++) {
    		data.append(toplot[i]);
    		if (i!=toplot.length-1) {
    			data.append(",");
    		}
     		else data.append("];");
    	}
    	data.append("\n 		var ts = [");
    	for (int i=0; i<toplot.length; i++) {
    		data.append(timestamps[i]);
    		if (i!=timestamps.length-1) {
    			data.append(",");
    		}
    		else data.append("];");
    	}
    		data.append(after.toString());
    		return data.toString();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plot);
        ArrayList<double[]> dataWhole=(ArrayList<double[]>)getIntent().getExtras().get("DATA");
        long[] ts=new long[dataWhole.size()];
        double[] data=new double[dataWhole.size()];
        for (int i=0; i<dataWhole.size(); i++) {
        	double[] datum=dataWhole.get(i);;
        	ts[i]=(long) datum[0];
        	data[i]=(Double) datum[1];
        }
        String html = generatePlot(ts, data);
        System.out.println(html);
        System.out.println("IVE GOT MY HTML WHATS UR PROB MAN?");
        String mime = "text/html";
        String encoding = "utf-8";

        WebView myWebView = (WebView)this.findViewById(R.id.plotview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        int scale = (int) (100 * myWebView.getScale());
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setSupportZoom(true);
        myWebView.setInitialScale(scale);    
        myWebView.loadDataWithBaseURL(null, html, mime, encoding, null);
    }
	protected long getRange() {
		// TODO Auto-generated method stub
		return 1000*60*60*24*30*3;
	}
	
	
}
