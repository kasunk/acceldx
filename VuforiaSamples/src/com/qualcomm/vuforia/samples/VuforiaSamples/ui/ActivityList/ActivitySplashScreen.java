/*==============================================================================
 Copyright (c) 2012-2013 Qualcomm Connected Experiences, Inc.
 All Rights Reserved.
 ==============================================================================*/

package com.qualcomm.vuforia.samples.VuforiaSamples.ui.ActivityList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.qualcomm.vuforia.samples.VuforiaSamples.Dashboard;
import com.qualcomm.vuforia.samples.VuforiaSamples.DatabaseHandler;
import com.qualcomm.vuforia.samples.VuforiaSamples.DatabaseInvokingActivity;
import com.qualcomm.vuforia.samples.VuforiaSamples.RegistrationActivity;

import com.qualcomm.vuforia.samples.VuforiaSamples.R;


public class ActivitySplashScreen extends DatabaseInvokingActivity
{
    
    private static long SPLASH_MILLIS = 450;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        LayoutInflater inflater = LayoutInflater.from(this);
        RelativeLayout layout = (RelativeLayout) inflater.inflate(
            R.layout.splash_screen, null, false);
        
        addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT));
        
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            
            @Override
            public void run()
            {
            	DatabaseHandler db = getDB();
            	String userName=db.getUsername();
            	System.out.println("GOT USERNAME: "+userName);
            	userName=null;
            	if (userName==null) {
            		Intent intent = new Intent(ActivitySplashScreen.this,
            								   RegistrationActivity.class);
            		startActivity(intent);
            	}
            	else {
            		Intent intent = new Intent(ActivitySplashScreen.this,
            								   Dashboard.class);
            		startActivity(intent);
		
            	}
                
            }
            
        }, SPLASH_MILLIS);
    }
    
}
