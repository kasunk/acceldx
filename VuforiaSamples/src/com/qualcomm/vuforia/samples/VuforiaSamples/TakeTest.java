package com.qualcomm.vuforia.samples.VuforiaSamples;



import com.qualcomm.vuforia.samples.VuforiaSamples.app.ImageTargets.ImageTargets;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TakeTest extends Activity {
	
	String secsElapsed;
	String minsElapsed;
	TextView timerText;
	TextView minsText;
	Button manageTest;
	private CountDownTimer cdTimer;
	String buttonState="NOT_CLICKED";
	
	
	ProgressWheel pb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setTitle("Test in Progress");
		 timerText=(TextView)findViewById(R.id.tv_timer);
		 minsText=(TextView)findViewById(R.id.tv_mins);
		
		 manageTest=(Button)findViewById(R.id.btn_cancel_test);
		 
		 
		 
		 cdTimer = new MyCountDownTimer(1000*60*20, 1);
		 cdTimer.start();
		 pb = (ProgressWheel)findViewById(R.id.progressBarTwo);
//			pb.setMax(100);
			
			pb.setProgress(0);
		 
		 
		 manageTest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				cdTimer.cancel();
				timerText.setTextColor(Color.parseColor("#FFFFFF"));
				minsText.setTextColor(Color.parseColor("#FFFFFF"));				
				manageTest.setBackgroundResource(R.drawable.take_pic_button);
				
				if (buttonState.equals("NOT_CLICKED")) {
					cdTimer.cancel();
					timerText.setTextColor(Color.parseColor("#FFFFFF"));
					minsText.setTextColor(Color.parseColor("#FFFFFF"));				
					manageTest.setBackgroundResource(R.drawable.take_pic_button);
					buttonState="CLICKED";
					setTitle("Test Canceled");
				} else {
					
					//call camera view here
					//change the class name
					Intent intent = new Intent(getBaseContext(),
	   				   ImageTargets.class);
					startActivity(intent);
					finish();
				}
			}
		});
		
	}
	
//	public class MyCountDownTimer extends CountDownTimer {
//
//		public MyCountDownTimer(long startTime, long interval) {
//
//			super(startTime, interval);
//
//		}
//
//		@Override
//		public void onFinish() {
//			timerText.setTextColor(Color.parseColor("#FFFFFF"));
//			minsText.setTextColor(Color.parseColor("#FFFFFF"));				
//			manageTest.setBackgroundResource(R.drawable.take_pic_button);
//			buttonState="CLICKED";
//			setTitle("Test Completed");
//		}
//
//		@Override
//		public void onTick(long millisUntilFinished) {
//
//			final long minutes_left = ((millisUntilFinished / 1000) / 60);
//		    final long seconds_left = (millisUntilFinished / 1000) - (minutes_left * 60);
//		    secsElapsed=String.valueOf(seconds_left);
//		    minsElapsed=String.valueOf(minutes_left);
//			timerText.setText(minsElapsed+":"+secsElapsed);
//			 
//		}
//
//	}
	
	public class MyCountDownTimer extends CountDownTimer {

		public MyCountDownTimer(long startTime, long interval) {

			super(startTime, interval);

		}

		@Override
		public void onFinish() {
			timerText.setTextColor(Color.parseColor("#FFFFFF"));
			minsText.setTextColor(Color.parseColor("#FFFFFF"));				
			manageTest.setBackgroundResource(R.drawable.take_pic_button);
			buttonState="CLICKED";
			setTitle("Test Completed");
		}

		@Override
		public void onTick(long millisUntilFinished) {

			final long minutes_left = ((millisUntilFinished / 1000) / 60);
		    final long seconds_left = (millisUntilFinished / 1000) - (minutes_left * 60);
		    secsElapsed=String.valueOf(seconds_left);
		    minsElapsed=String.valueOf(minutes_left);
		    
		    
//lines add by me
 //from		    
	    final long ss=  ((((1200-(millisUntilFinished/1000))/12)*360)/100);
	    timerText.setText(minsElapsed+":"+secsElapsed);
	    final int pre = Integer.valueOf(String.valueOf(ss));
	    pb.setProgress(pre);
//to	    
	    
			 
		}

	}

}
