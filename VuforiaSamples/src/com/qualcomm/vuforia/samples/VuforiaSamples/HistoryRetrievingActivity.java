package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.util.ArrayList;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.qualcomm.vuforia.samples.VuforiaSamples.Questionnaire.TestStarter;

public abstract class HistoryRetrievingActivity extends DatabaseInvokingActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		ArrayList<double[]> data=(ArrayList<double[]>)getIntent().getExtras().get("DATA");
		long range=getRange();
		double min=Double.MAX_VALUE;
		double max=Double.MIN_VALUE;
		double avg=0;
		range=System.currentTimeMillis()-range;
		for (int i=0; i<data.size(); i++) {
			double[] datum=(double[]) data.get(i);
			System.out.println("DATA: "+datum[0]+"..."+range+".."+data.get(i));
			if (datum[0]>range) {
				if (min>datum[1]) {
					min=datum[1];
				}
				if (max<datum[1]) {
					max=datum[1];
				}
				avg+=datum[1];
			}
		}
		if (data.size()>0) {
			avg/=(double) data.size();
		}
		TextView text=(TextView) findViewById(R.id.highVal);
		text.setText(""+Math.round(max)+" ng/ml");
		text=(TextView) findViewById(R.id.LowVal);
		text.setText(""+Math.round(min)+" ng/ml");
		text=(TextView) findViewById(R.id.avgVal);
		text.setText(""+Math.round(avg)+" ng/ml");
	}
	
	protected abstract long getRange();
}
