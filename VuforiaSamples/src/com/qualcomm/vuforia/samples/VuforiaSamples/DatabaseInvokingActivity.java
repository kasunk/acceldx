package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public abstract class DatabaseInvokingActivity extends Activity {
	
	protected ExecutorService executor = Executors.newFixedThreadPool(10);
	
	public SharedPreferences getPreferences() {
		return getBaseContext().getSharedPreferences("com.acceldx.sharedprefsfile", Context.MODE_PRIVATE);
	}
	
	public DatabaseHandler getDB() {
		return new DatabaseHandler(getBaseContext(), getPreferences());
	}
}
