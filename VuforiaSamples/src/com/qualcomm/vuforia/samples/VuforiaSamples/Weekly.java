package com.qualcomm.vuforia.samples.VuforiaSamples;

public class Weekly extends HistoryRetrievingActivity {

	@Override
	protected long getRange() {
		long retval=1000*60;
		retval*=60;
		retval*=24;
		retval*=7;
		return retval;
	}

}
