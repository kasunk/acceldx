package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.simple.parser.ParseException;

import com.qualcomm.vuforia.samples.VuforiaSamples.ui.ActivityList.ActivitySplashScreen;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends DatabaseInvokingActivity implements OnClickListener {
	boolean afterError=false;
	
	EditText passwordBox;
	EditText idBox;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
//		ImageButton loginButton = (ImageButton)findViewById(R.id.loginButton);
		setTitle("Login");
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		passwordBox   = (EditText)findViewById(R.id.ed_login_password1);
    	idBox  = (EditText)findViewById(R.id.ed_patient_id1);
    	passwordBox.setText("L1lv1c1h");
    	idBox.setText("design@eye-echo.com");
		
		Button loginButton=(Button)findViewById(R.id.btn_login);
		loginButton.setOnClickListener(this);
		EditText textBox = (EditText)findViewById(R.id.ed_patient_id1);
		textBox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (afterError) {
					EditText box=(EditText) arg0;
					box.setText("");
					afterError=false;	
				}
			}
		});
//		TextView wait=(TextView)findViewById(R.id.notification);
//		wait.setVisibility(View.INVISIBLE);
	}

	private boolean verify(final String id, final String pwd) {
		System.out.println("VERIFYING "+id+" .. "+pwd);
		boolean retval=false;
//		TextView wait=(TextView)findViewById(R.id.notification);
//		wait.setVisibility(View.VISIBLE);
//		wait.invalidate();
		Future future=executor.submit(new Callable<Boolean>() {
			public Boolean call() {
				try {
					Boolean value=SharefileAPI.api().authenticate("acceldx", "sharefile.com", id, pwd);
					System.out.println("GOT BACK: "+value);
					if (value) {
						HashMap<String, String> folders=SharefileAPI.api().folderList("");
						if (!folders.containsKey(id)) {
							value=SharefileAPI.api().createFolder(id,"");
							value=SharefileAPI.api().createFolder("images",id);
							value=SharefileAPI.api().createFolder("questionnaires",id);
						}
					}
					return value;
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return false;
			}
		});
		try {
			retval= (Boolean) future.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		wait.setVisibility(View.INVISIBLE);
//		wait.invalidate();
		return retval;
	}
	
	@Override
	public void onClick(View button) {
    	
    	

		if (isOnline()) {
			
		
    	
    	if (verify(idBox.getText().toString(), passwordBox.getText().toString())) {
    		DatabaseHandler db = getDB();
    		db.createUser(idBox.getText().toString(),passwordBox.getText().toString());
    		Intent intent = new Intent(getBaseContext(),
				   				   PatientActivity.class);
    		
    		
    		startActivity(intent);
    		this.finish();
    	}
    	else {
    		passwordBox.setHint("");
    		idBox.setHint("Wrong User ID");
    		
    		
    		passwordBox.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.wrong_pw_icon), null);
    		passwordBox.setHintTextColor(getResources().getColor(R.color.wrong_login_color));
    		passwordBox.setBackgroundResource(R.drawable.edit_text_border);
    		
    		idBox.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.wrong_patient_id_icon), null);
    		idBox.setBackgroundResource(R.drawable.edit_text_border);
    		idBox.setHintTextColor(getResources().getColor(R.color.wrong_login_color));
    		
//    		ImageView img=(ImageView)findViewById(R.id.idImage);
//    		int resID = getResources().getIdentifier("wrong_patient_id_icon", "drawable",  getPackageName());
//    		img.setImageResource(resID);
//    		img=(ImageView)findViewById(R.id.pwdImage);
//    		resID = getResources().getIdentifier("wrong_pw_icon", "drawable",  getPackageName());
//    		img.setImageResource(resID);
    		afterError=true;
    	}
    	
		}else{
			Toast.makeText(getApplicationContext(), "Please check the internet connection", Toast.LENGTH_LONG).show();
		}
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm =(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnected()) {
	        return true;
	    }
	    return false;
	}
	
	
}
