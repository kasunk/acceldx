package com.qualcomm.vuforia.samples.VuforiaSamples;


import java.util.ArrayList;

import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class History extends TabActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.historytabs);
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	StringBuilder dataFile=SharefileAPI.api().getData();
	String csvData=dataFile.toString();
	ArrayList<double[]> data=new ArrayList<double[]>();
	String[] lines=csvData.split("\\r?\\n");
	for (int i=0; i<lines.length; i++) {
		try{
		System.out.println(lines[i]);
		String[] parts=lines[i].split(",");
		double[] datum=new double[2];
		for (int j=0; j<datum.length; j++) {
			datum[j]=Double.parseDouble(parts[j]);
		}
		data.add(datum);
		}
		catch(Exception ignore) {
			
		}
	}
	/** TabHost will have Tabs */
	TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);

	/** TabSpec used to create a new tab.
	* By using TabSpec only we can able to setContent to the tab.
	* By using TabSpec setIndicator() we can set name to tab. */

	/** tid1 is firstTabSpec Id. Its used to access outside. */
	TabSpec firstTabSpec = tabHost.newTabSpec("tid1");
	TabSpec secondTabSpec = tabHost.newTabSpec("tid1");
	TabSpec thirdTabSpec = tabHost.newTabSpec("tid1");

	/** TabSpec setIndicator() is used to set name for the tab. */
	/** TabSpec setContent() is used to set content for a particular tab. */
	Intent weekly=new Intent(this,Weekly.class);
	weekly.putExtra("DATA", data);
	firstTabSpec.setIndicator("Weekly").setContent(weekly);
	Intent monthly=new Intent(this,Monthly.class);
	monthly.putExtra("DATA", data);
	secondTabSpec.setIndicator("Monthly").setContent(monthly);
	Intent monthly3=new Intent(this,Monthly3.class);
	monthly3.putExtra("DATA", data);
	thirdTabSpec.setIndicator("Three Month").setContent(monthly3);

	/** Add tabSpec to the TabHost to display. */
	tabHost.addTab(firstTabSpec);
	tabHost.addTab(secondTabSpec);
	tabHost.addTab(thirdTabSpec);

	}
}
