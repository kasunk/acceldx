package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Result extends DatabaseInvokingActivity {
	String imageFile;
	EditText first;
	 EditText second;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results);
		Button submit = (Button)findViewById(R.id.submitNext);
		submit.setOnClickListener(new TestSubmitter());
		first=(EditText) findViewById(R.id.result1);
	    first.setHint("Test Column");
	    second=(EditText) findViewById(R.id.result2);
	    second.setHint("Control Column");
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    String pictureLoc=getIntent().getExtras().getString("pictureTaken");
		final File imgFile = new  File(pictureLoc);
		Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		ImageView myImage = (ImageView) findViewById(R.id.imageView1);
//	    myImage.setImageBitmap(myBitmap);
	    
	    
	    Matrix matrix = new Matrix();

	    matrix.postRotate(90);

//	    Bitmap scaledBitmap = Bitmap.createScaledBitmap(myBitmap,50,50,true);
	    
	    Bitmap scaledBitmap = Bitmap.createBitmap(myBitmap);

	    Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

	    myImage.setImageBitmap(rotatedBitmap);
	    
	    setTitle("Confirm Image");
	}

	class TestSubmitter implements OnClickListener {
	@Override
	public void onClick(View source) {
		
		if(!(first.getText().toString().isEmpty())&&(!second.getText().toString().isEmpty()))
		{
			
		
		
		if (getIntent().getExtras()!=null) {
			String pictureLoc=getIntent().getExtras().getString("pictureTaken");
			final String qrCode=getIntent().getExtras().getString("QRCodeText");
			System.out.println("RECIEVED LOCATION: "+pictureLoc);
			if (pictureLoc!=null) {
				imageFile=pictureLoc;
				final File imgFile = new  File(imageFile);
				if (imgFile.exists()) {
				    EditText first=(EditText) findViewById(R.id.result1);
				    final double firstValue=Double.parseDouble(first.getText().toString());
				    first=(EditText) findViewById(R.id.result2);
				    final double secondValue=Double.parseDouble(first.getText().toString());
				    final double thirdValue=((firstValue-secondValue)*30);
				    final long time=System.currentTimeMillis();
					Button submit = (Button)findViewById(R.id.submitNext);
					submit.setText("Please wait, saving..");

				    try {
				    	//getDB().addData(imgFile, Long.parseLong(imgFile.getName()), firstValue, secondValue);
				    	Future future=executor.submit(new Callable<Boolean>() {
							public Boolean call() {
				    			return SharefileAPI.api().saveData(getDB().getDeviceID(), qrCode, time, new double[]{firstValue, secondValue, thirdValue});
							}
				    	});
				    	future.get();
				    } catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    try {
				    	//getDB().addData(imgFile, Long.parseLong(imgFile.getName()), firstValue, secondValue);
				    	Future future=executor.submit(new Callable<Boolean>() {
							public Boolean call() {
				    			return SharefileAPI.api().saveFile(getDB().getDeviceID(), qrCode, time, imgFile);
							}
				    	});
				    	future.get();
				    } catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    
				}
			}
		}

		Intent intent = new Intent(getBaseContext(),
	   				   				   Questionnaire.class);
		startActivity(intent);
		Result.this.finish();
		
		
		
		}
		else{
			Toast.makeText(getApplicationContext(), "Enter Values", Toast.LENGTH_LONG).show();
		}
		}
	}
}
