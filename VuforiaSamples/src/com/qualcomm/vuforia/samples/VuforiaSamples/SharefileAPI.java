package com.qualcomm.vuforia.samples.VuforiaSamples;
import java.net.*;
import java.util.*;
import java.util.Map.Entry;
import java.io.*;

import org.json.simple.*;
import org.json.simple.parser.*;

public class SharefileAPI {
	String subdomain;
	String tld;
	String authId;
	String username;
	String imageFolder="";
	String dataFolder="";
	String dataFile="";
	String questionnaireFolder="";
	StringBuilder existing=new StringBuilder();
	String patientID;

	private static SharefileAPI _api=null;
	
	public static SharefileAPI api() {
		if (_api==null) {
			_api=new SharefileAPI();
		}
		return _api;
	}
	
	/**
	 * Calls getAuthID to retrieve an authid that will be used for subsequent calls to API. 
     *
     * If you normally login to ShareFile at an address like https://mycompany.sharefile.com,
     * then your subdomain is mycompany and your tld is sharefile.com
     *
     * sample.authenticate("mycompany", "sharefile.com", "my@user.name", "mypassword");
     *
	 * @param subdomain
	 * @param tld
	 * @param username
	 * @param password
	 * @return boolean true if authentication was successful, false otherwise
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public boolean authenticate(String subdomain, String tld, String username, String password)
	throws MalformedURLException, IOException, ParseException
	{	
		this.subdomain = subdomain; 
		this.tld = tld;
		this.username=username;
		
		String requestUrl = String.format("https://%s.%s/rest/getAuthID.aspx?fmt=json&username=%s&password=%s",
				subdomain, tld, URLEncoder.encode(username, "UTF-8"), URLEncoder.encode(password, "UTF-8"));
		
		System.out.println(requestUrl);

		JSONObject jsonObj = this.invokeShareFileOperation(requestUrl);
		Boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			String authId = (String) jsonObj.get("value");
			System.out.println("authid="+authId);
			this.authId = authId;
			questionnaireFolder=getID("/pScreenApp/"+username,"questionnaires");
			return(true);
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
			return(false);
		}        
	}
	
	/**
	 * Prints out a folder list for the specified path or root if none is provided.
     *
     * Currently prints out id, filename, creationdate, type.
     *
	 * @param path folder to list
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public boolean createFolder(String path, String base)
	throws MalformedURLException, IOException, ParseException
	{
		if (base.isEmpty()) {
			base="/pScreenApp";
		}
		else {
			base="/pScreenApp/"+base;
		}
		if (path.isEmpty()) {
			return false;
		}

		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("name", path);
		requiredParameters.put("path", base);

		String url = this.buildUrl("folder", "create", requiredParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		Boolean error = (Boolean) jsonObj.get("error");
		if (error) {
			Long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
			return false;
		}
		return true;
	}
	
	private String getID(String base, String folderName) throws MalformedURLException, IOException, ParseException {
		HashMap<String,String> locations=folderList(base);
		System.out.println("LOOKING FOR "+folderName+".. in.."+locations);
		return locations.get(folderName);
	}
	
	public StringBuilder getData() {
		return existing;
	}
	
	public boolean initialiseUser(String patientID) {
		boolean retval=false;	
		this.patientID=patientID;
		System.out.println("INSIDE INITIALISE USER");
		try {
				dataFolder=getID("/pScreenApp",username);
				System.out.println("GOT BACK dataFOLDER: "+dataFolder);
				dataFile=getID("/pScreenApp/"+username, patientID+"_data.csv");
				System.out.println("GOT BACK dataFILE: "+dataFile);
				if (dataFile==null) {
					HashMap<String, Object> params=new HashMap<String,Object>();
					params.put("folderid", dataFolder);
					System.out.println("CALLING TEXTUPLOAD)");
					textUpload("timestamp, data-1, data-2, data-3, time in GMT, device-id, QR code", patientID+"_data.csv", params);
					System.out.println("CALLING DATA DOWNLOAD)");
					dataFile=getID("/pScreenApp/"+username, patientID+"_data.csv");
					dataDownload(dataFile);
					System.out.println("RETURNED: "+existing.toString());
					retval=true;
				}
				else {
					dataDownload(dataFile);
					retval=true;
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return retval;
	}
	
	public boolean saveFile(String deviceID, String qrCode, long timestamp, File image) {
		if (imageFolder==null || imageFolder.length()<2) {
			try {
				imageFolder=getID("/pScreenApp/"+username,"images");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		HashMap<String, Object> params=new HashMap<String, Object>();
		qrCode = qrCode.replaceAll("[^a-zA-Z0-9.-]", "_");
		params.put("folderid", imageFolder);
		try {
			System.out.println("SAVING IMAGE AS "+patientID+"_"+deviceID+"_"+timestamp+".png");
			fileUpload(image.getCanonicalPath(), patientID+"_"+deviceID+"_"+qrCode+"_"+timestamp+".png",params);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean saveQuestionnaire(String deviceID, long timestamp, HashMap<String, String> answers) {
		boolean retval=false;
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("overwrite", "true");
		Date readableDate=new Date(timestamp);
		StringBuilder builder=new StringBuilder("");
		Iterator iterate=answers.entrySet().iterator();
		while (iterate.hasNext()) {
			Entry<String,String> answer=(Entry<String,String>)iterate.next();
			builder.append(answer.getKey());
			builder.append("->");
			builder.append(answer.getValue());
			builder.append("\n");
		}
		try {
			params.put("folderid", questionnaireFolder);
			String newText=builder.toString();
			textUpload(newText, patientID+"_"+readableDate.toGMTString()+".txt", params);
			System.out.println("QUESTIONNAIRE: "+newText);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("RETURNING FROM FILE UPLOAD");
		return retval;
	}

	
	public boolean saveData(String deviceID, String qrCode, long timestamp, double[] values) {
		boolean retval=false;
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("overwrite", "true");
		qrCode = qrCode.replaceAll("[^a-zA-Z0-9.-]", "_");
		Date readableDate=new Date(timestamp);
		try {
//			existing.append("\n");
			existing.append(timestamp);
			existing.append(",");
			existing.append(values[0]);
			existing.append(",");
			existing.append(values[1]);
			existing.append(",");
			existing.append(values[2]);
			existing.append(",");
			existing.append(readableDate.toGMTString());
			existing.append(",");
			existing.append(deviceID);
			existing.append(",");
			existing.append(qrCode);
			params.put("folderid", dataFolder);
			String newText=existing.toString();
			textUpload(newText, patientID+"_data.csv", params);
			System.out.println("DATA: "+newText);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("RETURNING FROM FILE UPLOAD");
		return retval;
	}
	

	/**
	 * Prints out a folder list for the specified path or root if none is provided.
     *
     * Currently prints out id, filename, creationdate, type.
     *
	 * @param path folder to list
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public HashMap<String, String> folderList(String path)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, String>  retval=new HashMap<String,String>();
		if (path.isEmpty()) {
			path = "/pScreenApp";
		}

		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("path", path);

		String url = this.buildUrl("folder", "list", requiredParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		Boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			JSONArray items = (JSONArray) jsonObj.get("value");
			if (items.isEmpty()) {
				System.out.println("No 	Results");
			}
			Iterator<?> iterItems = items.iterator();
			while (iterItems.hasNext()) {
				JSONObject item = (JSONObject) iterItems.next();
				System.out.println(item.get("id")+" "+item.get("filename")+" "+item.get("creationdate")+" "+item.get("type"));
				if (item!=null && item.get("filename")!=null) {
					retval.put(item.get("filename").toString(), item.get("id").toString());
				}
			}
		}
		else {
			Long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}
		return retval;
	}

	/**
	 * Uploads a file to ShareFile.
	 * 
	 * @param localPath full path to local file like "c:\\path\\to\\file.txt"
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void textUpload(String data, String filename, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("filename", filename);

		String url = this.buildUrl("file", "upload", requiredParameters, optionalParameters);
		System.out.println(url);
		
		JSONObject jsonObj = this.invokeShareFileOperation(url);
		Boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			String uploadUrl = (String)jsonObj.get("value");
			System.out.println("uploadUrl = "+uploadUrl);
			InputStream source = new ByteArrayInputStream(data.getBytes("UTF-8"));

			this.multipartUploadFile(source,filename,uploadUrl);
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}
		
	}

	
	
	/**
	 * Uploads a file to ShareFile.
	 * 
	 * @param localPath full path to local file like "c:\\path\\to\\file.txt"
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void fileUpload(String localPath, String filename, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("filename", new File(localPath).getName());

		String url = this.buildUrl("file", "upload", requiredParameters, optionalParameters);
		System.out.println(url);
		
		JSONObject jsonObj = this.invokeShareFileOperation(url);
		Boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			String uploadUrl = (String)jsonObj.get("value");
			System.out.println("uploadUrl = "+uploadUrl);
			File file=new File(localPath);
			InputStream source = new FileInputStream(file);
			if (filename.isEmpty()) {
					filename=file.getName();
			}
			this.multipartUploadFile(source,filename, uploadUrl);
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}
		
	}

	
	/**
	 * Downloads a file from ShareFile.
	 * 
	 * @param fileId id of the file to download
	 * @param localPath complete path to download file to including filename
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void dataDownload(String fileId)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("id", fileId);

		String url = this.buildUrl("file", "download", requiredParameters);
		System.out.println("DOWNLOADING DATA: "+url);
		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			String downloadUrl = (String)jsonObj.get("value");
			System.out.println("downloadUrl = "+downloadUrl);
			existing=new StringBuilder();
			BufferedReader source = null;
			try {
				source = new BufferedReader(new InputStreamReader((new URL(downloadUrl).openStream())));
				String inputLine;

		        while ((inputLine = source.readLine()) != null) {
		            existing.append(inputLine);
		            existing.append("\n");
		            System.out.println(inputLine);
		        }

				System.out.println("Download complete..");
			}
			catch(IOException ioe){
				ioe.printStackTrace();
			}
			finally {
				source.close();
			}
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}

	}
	
	/**
	 * Downloads a file from ShareFile.
	 * 
	 * @param fileId id of the file to download
	 * @param localPath complete path to download file to including filename
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void fileDownload(String fileId, String localPath)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("id", fileId);

		String url = this.buildUrl("file", "download", requiredParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			String downloadUrl = (String)jsonObj.get("value");
			System.out.println("downloadUrl = "+downloadUrl);
			
			BufferedInputStream source = null;
			FileOutputStream target = null;
			try {
				source = new BufferedInputStream(new URL(downloadUrl).openStream());
				target = new FileOutputStream(localPath);

    			byte chunk[] = new byte[8192];
    			int len;
    			while ((len = source.read(chunk, 0, 8192)) != -1)
    			{
    				target.write(chunk, 0, len);
    			}
    			System.out.println("Download complete.");
			}
			catch(IOException ioe){
				ioe.printStackTrace();
			}
			finally {
				source.close();
				target.close();
			}
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}

	}
	
	/**
	 * Sends a Send a File email.
	 * 
	 * @param path path to file in ShareFile to send
	 * @param to email address to send to
	 * @param subject email subject 
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void fileSend(String path, String to, String subject, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("path", path);
		requiredParameters.put("to", to);
		requiredParameters.put("subject", subject);
		
		String url = this.buildUrl("file", "send", requiredParameters, optionalParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			System.out.println(jsonObj.get("value"));
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}
	}
	
	/**
	 * Creates a client or employee user in ShareFile.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param isEmployee true to create an employee, false to create a client
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void usersCreate(String firstName, String lastName, String email, Boolean isEmployee, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("firstname", firstName);
		requiredParameters.put("lastname", lastName);
		requiredParameters.put("email", email);
		requiredParameters.put("isemployee", isEmployee);
		
		String url = this.buildUrl("users", "create", requiredParameters, optionalParameters);
		System.out.println(url);
		
		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			System.out.println(jsonObj.get("value"));
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}		
	}	

	/**
	 * Creates a distribution group in ShareFile.
	 * 
	 * Ex: to create a group and add users to it at the same time
	 * 
	 * optionalParameters.put("isshared", true);
	 * optionalParameters.put("contacts", "an@email.address,another@email.address");
	 * sample.groupCreate("MyGroupName", optionalParameters);
	 * 
	 * @param name
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void groupCreate(String name, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("name", name);
		
		String url = this.buildUrl("group", "create", requiredParameters, optionalParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			System.out.println(jsonObj.get("value"));
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}			
	}

	/**
	 * Searches for items in ShareFile.
	 * 
	 * @param query
	 * @param optionalParameters HashMap of optional parameter names/values
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public void search(String query, HashMap<String, Object> optionalParameters)
	throws MalformedURLException, IOException, ParseException
	{
		HashMap<String, Object> requiredParameters = new HashMap<String, Object>();
		requiredParameters.put("query", query);
		
		String url = this.buildUrl("search", "search", requiredParameters, optionalParameters);
		System.out.println(url);

		JSONObject jsonObj = this.invokeShareFileOperation(url);
		boolean error = (Boolean) jsonObj.get("error");
		if (!error) {
			JSONArray items = (JSONArray) jsonObj.get("value");
			if (items.isEmpty()) {
				System.out.println("No Results");
			}
			Iterator<?> iterItems = items.iterator();
			String path = "";
			while (iterItems.hasNext()) {
				JSONObject item = (JSONObject) iterItems.next();
                path = "/";
                if(item.get("parentid").equals("box")) {
                    path = "/File Box";
                }
                else {
                    path = (String)item.get("parentsemanticpath");
                }
				System.out.println(path+"/"+item.get("filename")+" "+item.get("creationdate")+" "+item.get("type"));
			}
		}
		else {
			long errorCode = (Long) jsonObj.get("errorCode");
			String errorMessage = (String) jsonObj.get("errorMessage");
			System.out.println(errorCode + " : " + errorMessage);
		}			
	}

	
	/***************************** Helper Operations *****************************/
	private JSONObject invokeShareFileOperation(String requestUrl)
	throws MalformedURLException, IOException, ParseException
	{
		URL url = new URL(requestUrl);
		URLConnection connection = url.openConnection();  
		connection.connect();
		InputStream is = connection.getInputStream();
	
		int read = -1;
		byte[] inbuffer = new byte[4096];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		 
		while((read = is.read(inbuffer)) != -1){
			baos.write(inbuffer, 0, read);
		}
		
		byte [] b = baos.toByteArray();
	    is.close();
	    
        JSONParser parser=new JSONParser();
        return ((JSONObject) parser.parse(new String(b)));
	}

	private String buildUrl(String endpoint, String op, HashMap<String, Object> requiredParameters)
	{
		return this.buildUrl(endpoint, op, requiredParameters, new HashMap<String, Object>());
	}
	
	private String buildUrl(String endpoint, String op, HashMap<String, Object> requiredParameters, HashMap<String, Object> optionalParameters)
	{
		requiredParameters.put("authid", this.authId);
		requiredParameters.put("op", op);
		requiredParameters.put("fmt", "json");
		ArrayList<String> parameters = new ArrayList<String>();
		StringBuilder urlParameters = new StringBuilder();
		
		try {
			System.out.println("Processing params: "+requiredParameters);
			for (Map.Entry<String, Object> entry : requiredParameters.entrySet()) {
				System.out.println("Encoding: "+entry);
				parameters.add(String.format("%s=%s", entry.getKey(), URLEncoder.encode(entry.getValue().toString(), "UTF-8")));
			}
			System.out.println("Processing params: "+optionalParameters);
			for (Map.Entry<String, Object> entry : optionalParameters.entrySet()) {
				System.out.println("Encoding: "+entry);
				parameters.add(String.format("%s=%s", entry.getKey(), URLEncoder.encode(entry.getValue().toString(), "UTF-8")));
			}
		
			String separator = "";

			for(String param : parameters) {
				urlParameters.append(separator);
				urlParameters.append(param);
				separator = "&";
			}
		}
		catch (UnsupportedEncodingException ue) {
			ue.printStackTrace();
		}
		String url = String.format("https://%s.%s/rest/%s.aspx?%s", this.subdomain, this.tld, endpoint, urlParameters);
		return(url);
	}

	private void multipartUploadFile(InputStream source, String filename, String uploadUrl)
	throws MalformedURLException, IOException
	{
		URL url = new URL(uploadUrl);
		URLConnection connection = url.openConnection();

		String boundary = "--"+UUID.randomUUID().toString();

		connection.setDoOutput(true);
	    connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		
		
		OutputStream target = connection.getOutputStream();
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("--"+boundary+"\r\n");
		buffer.append("Content-Disposition: form-data; name=File1; filename=\""+filename+"\"\r\n");		
	    String contentType = URLConnection.guessContentTypeFromName(filename);
	    if (contentType == null) { contentType = "application/octet-stream"; }
		buffer.append("Content-Type: "+contentType+"\r\n\r\n");

		target.write(buffer.toString().getBytes());
		
		// read from file, and write to outputstream
	    byte[] buf = new byte[1024*1024];
	    int len;
	    while((len = source.read(buf, 0, buf.length)) >= 0) {
	    	target.write(buf, 0, len);
	    }
	    target.flush();
	    
		target.write(("\r\n--"+boundary+"--\r\n").getBytes());
		
		// get Response
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuffer response = new StringBuffer();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        response.append(line).append("\n");
	    }
	    reader.close();
	    System.out.println(response.toString());
	    
		target.close();
		source.close();
	}	

}
