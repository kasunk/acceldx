package com.qualcomm.vuforia.samples.VuforiaSamples;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.qualcomm.vuforia.samples.VuforiaSamples.Dashboard.TestStarter;
import com.qualcomm.vuforia.samples.VuforiaSamples.app.ImageTargets.ImageTargets;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Questionnaire extends DatabaseInvokingActivity {
	
	TextView bar1Num0;
	TextView bar1Num1;
	TextView bar1Num2;
	TextView bar1Num3;
	TextView bar1Num4;
	TextView bar1Num5;
	
	TextView bar2Num0;
	TextView bar2Num1;
	TextView bar2Num2;
	TextView bar2Num3;
	TextView bar2Num4;
	TextView bar2Num5;
	
	TextView bar3Num0;
	TextView bar3Num1;
	TextView bar3Num2;
	TextView bar3Num3;
	TextView bar3Num4;
	TextView bar3Num5;
	
	CheckBox cb1bar1;
	CheckBox cb2bar2;
	CheckBox cb3bar3;
	
	SeekBar sb1;
	SeekBar sb2;
	SeekBar sb3;
	
	int ans1=0;
	int ans2=0;
	int ans3=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		Button takeTest = (Button)findViewById(R.id.bt_submit);
		
		sb1 = (SeekBar)findViewById(R.id.test_seekbar1);
		sb2 = (SeekBar)findViewById(R.id.sb2);
		sb3 = (SeekBar)findViewById(R.id.sb3);
		
		sb1.setMax(100);
		sb2.setMax(100);
		sb3.setMax(100);
		
		bar1Num0=(TextView)findViewById(R.id.tv_1_0);
        bar1Num1=(TextView)findViewById(R.id.tv_1_1);
        bar1Num2=(TextView)findViewById(R.id.tv_1_2);
        bar1Num3=(TextView)findViewById(R.id.tv_1_3);
        bar1Num4=(TextView)findViewById(R.id.tv_1_4);
        bar1Num5=(TextView)findViewById(R.id.tv_1_5);
        
        bar2Num0=(TextView)findViewById(R.id.tv_2_0);
        bar2Num1=(TextView)findViewById(R.id.tv_2_1);
        bar2Num2=(TextView)findViewById(R.id.tv_2_2);
        bar2Num3=(TextView)findViewById(R.id.tv_2_3);
        bar2Num4=(TextView)findViewById(R.id.tv_2_4);
        bar2Num5=(TextView)findViewById(R.id.tv_2_5);
        
        bar3Num0=(TextView)findViewById(R.id.tv_3_0);
        bar3Num1=(TextView)findViewById(R.id.tv_3_1);
        bar3Num2=(TextView)findViewById(R.id.tv_3_2);
        bar3Num3=(TextView)findViewById(R.id.tv_3_3);
        bar3Num4=(TextView)findViewById(R.id.tv_3_4);
        bar3Num5=(TextView)findViewById(R.id.tv_3_5);
        
        cb1bar1=(CheckBox)findViewById(R.id.cb1_for_bar1);
        cb2bar2=(CheckBox)findViewById(R.id.cb2_for_bar2);
        cb3bar3=(CheckBox)findViewById(R.id.cb3_for_bar3);
		
		takeTest.setOnClickListener(new TestStarter());
		
		sb1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               if (progress<10) {
            	   sb1.setProgress(0);
            	   setBar1NumColorDefault(0);
            	   cb1bar1.setChecked(false);
            	   ans1=0;
               }else if ((10<=progress) && (progress<30)) {
            	   sb1.setProgress(20);
            	   setBar1NumColorDefault(1);
            	   cb1bar1.setChecked(true);
            	   ans1=1;
               }else if ((30<=progress) && (progress<50)) {
            	   sb1.setProgress(40);
            	   setBar1NumColorDefault(2);
            	   ans1=2;
            	   cb1bar1.setChecked(true);
               }else if (50<=progress && progress<70) {
            	   sb1.setProgress(60);
            	   setBar1NumColorDefault(3);
            	   cb1bar1.setChecked(true);
            	   ans1=3;
               }else if (70<=progress && progress<90) {
            	   sb1.setProgress(80);
            	   setBar1NumColorDefault(4);
            	   cb1bar1.setChecked(true);
            	   ans1=4;
               }else if (90<=progress) {
            	   sb1.setProgress(100);
            	   setBar1NumColorDefault(5);
            	   cb1bar1.setChecked(true);
            	   ans1=5;
               }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });
		sb2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               if (progress<10) {
            	   sb2.setProgress(0);
            	   setBar2NumColorDefault(0);
            	   cb2bar2.setChecked(false);
            	   ans2=0;
               }else if ((10<=progress) && (progress<30)) {
            	   sb2.setProgress(20);
            	   setBar2NumColorDefault(1);
            	   cb2bar2.setChecked(true);
            	   ans2=1;
               }else if ((30<=progress) && (progress<50)) {
            	   sb2.setProgress(40);
            	   setBar2NumColorDefault(2);
            	   cb2bar2.setChecked(true);
            	   ans2=2;
               }else if (50<=progress && progress<70) {
            	   sb2.setProgress(60);
            	   setBar2NumColorDefault(3);
            	   cb2bar2.setChecked(true);
            	   ans2=3;
               }else if (70<=progress && progress<90) {
            	   sb2.setProgress(80);
            	   setBar2NumColorDefault(4);
            	   cb2bar2.setChecked(true);
            	   ans2=4;
               }else if (90<=progress) {
            	   sb2.setProgress(100);
            	   setBar2NumColorDefault(5);
            	   cb2bar2.setChecked(true);
            	   ans2=5;
               }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });
		
		sb3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               if (progress<10) {
            	   sb3.setProgress(0);
            	   setBar3NumColorDefault(0);
            	   cb3bar3.setChecked(false);
            	   ans3=0;
               }else if ((10<=progress) && (progress<30)) {
            	   sb3.setProgress(20);
            	   setBar3NumColorDefault(1);
            	   cb3bar3.setChecked(true);
            	   ans3=1;
               }else if ((30<=progress) && (progress<50)) {
            	   sb3.setProgress(40);
            	   setBar3NumColorDefault(2);
            	   cb3bar3.setChecked(true);
            	   ans3=2;
               }else if (50<=progress && progress<70) {
            	   sb3.setProgress(60);
            	   setBar3NumColorDefault(3);
            	   cb3bar3.setChecked(true);
            	   ans3=3;
               }else if (70<=progress && progress<90) {
            	   sb3.setProgress(80);
            	   setBar3NumColorDefault(4);
            	   cb3bar3.setChecked(true);
            	   ans3=4;
               }else if (90<=progress) {
            	   sb3.setProgress(100);
            	   setBar3NumColorDefault(5);
            	   cb3bar3.setChecked(true);
            	   ans3=5;
               }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

        });
	}
	
	public void setBar1NumColorDefault(int num){
    	if(num==0){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==1){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#33FF33"));
    		bar1Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==2){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num2.setTextColor(Color.parseColor("#CCCC00"));
    		bar1Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==3){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num3.setTextColor(Color.parseColor("#CC6600"));
    		bar1Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==4){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num4.setTextColor(Color.parseColor("#CC3300"));
    		bar1Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==5){
    		bar1Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar1Num5.setTextColor(Color.parseColor("#CC0000"));
    	}
    }
	public void setBar2NumColorDefault(int num){
    	if(num==0){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==1){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#33FF33"));
    		bar2Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==2){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num2.setTextColor(Color.parseColor("#CCCC00"));
    		bar2Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==3){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num3.setTextColor(Color.parseColor("#CC6600"));
    		bar2Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==4){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num4.setTextColor(Color.parseColor("#CC3300"));
    		bar2Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==5){
    		bar2Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar2Num5.setTextColor(Color.parseColor("#CC0000"));
    	}
    }
	public void setBar3NumColorDefault(int num){
    	if(num==0){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==1){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#33FF33"));
    		bar3Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==2){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num2.setTextColor(Color.parseColor("#CCCC00"));
    		bar3Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==3){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num3.setTextColor(Color.parseColor("#CC6600"));
    		bar3Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==4){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num4.setTextColor(Color.parseColor("#CC3300"));
    		bar3Num5.setTextColor(Color.parseColor("#FFFFFF"));
    	}else if(num==5){
    		bar3Num0.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num2.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num3.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num4.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num1.setTextColor(Color.parseColor("#FFFFFF"));
    		bar3Num5.setTextColor(Color.parseColor("#CC0000"));
    	}
    }

	class TestStarter implements OnClickListener {
	@Override
	public void onClick(View source) {
	    try {
	    	//getDB().addData(imgFile, Long.parseLong(imgFile.getName()), firstValue, secondValue);
	    	final HashMap<String, String> values=new HashMap<String, String>();
	    	
	    	String q1="chest pain";
	    	String q2="difficult breathing";
	    	String q3="swelling";
	    	
	    	
//	    	TextView question;
//	    	SeekBar answer;
//	    	question=(TextView) findViewById(R.id.q1);
//	    	answer=(SeekBar) findViewById(R.id.a1);
	    	values.put(q1, ""+ans1);
//	    	question=(TextView) findViewById(R.id.q2);
//	    	answer=(SeekBar) findViewById(R.id.a2);
	    	values.put(q2, ""+ans2);
//	    	question=(TextView) findViewById(R.id.q3);
//	    	answer=(SeekBar) findViewById(R.id.a3);
	    	values.put(q3, ""+ans3);
	    	EditText wanswer=(EditText) findViewById(R.id.et_weight);
	    	values.put("body weight", wanswer.getText().toString());
	    	Future future=executor.submit(new Callable<Boolean>() {
				public Boolean call() {
	    			return SharefileAPI.api().saveQuestionnaire(getDB().getDeviceID(), System.currentTimeMillis(), values);
				}
	    	});
	    	future.get();
	    } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Intent i = new Intent(Questionnaire.this, Dashboard.class);
		i.setAction(Intent.ACTION_MAIN);
		i.addCategory(Intent.CATEGORY_LAUNCHER);
		startActivity(i);
		Questionnaire.this.finish();
	}
	}
}
